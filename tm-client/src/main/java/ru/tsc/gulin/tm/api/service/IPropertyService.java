package ru.tsc.gulin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.component.ISaltProvider;
import ru.tsc.gulin.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
