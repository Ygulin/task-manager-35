package ru.tsc.gulin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.gulin.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gulin.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gulin.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.gulin.tm.exception.field.TaskIdEmptyException;
import ru.tsc.gulin.tm.exception.field.UserIdEmptyException;
import ru.tsc.gulin.tm.repository.ProjectRepository;
import ru.tsc.gulin.tm.repository.TaskRepository;

import java.util.UUID;

public class ProjectTaskServiceTest {

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @Before
    public void init() {
        projectRepository.create(USER_ID_1, "project_1", "project_1");
        taskRepository.create(USER_ID_1, "task_1", "task_1");
        taskRepository.create(USER_ID_1, "task_2", "task_2");

    }

    @Test
    public void bindTaskToProject() {
        @NotNull String projectId = projectRepository.findOneByIndex(USER_ID_1, 0).getId();
        @NotNull String taskId = taskRepository.findOneByIndex(USER_ID_1, 0).getId();
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject("", projectId, taskId)
        );
        Assert.assertThrows(ProjectIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID_1, "", taskId)
        );
        Assert.assertThrows(TaskIdEmptyException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID_1, projectId, "")
        );
        Assert.assertThrows(ProjectNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID_1, "123", taskId)
        );
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID_1, projectId, "123")
        );
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertNotNull(taskRepository.findOneByIndex(USER_ID_1, 0).getProjectId());
        Assert.assertEquals(projectId, taskRepository.findOneByIndex(USER_ID_1, 0).getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull String projectId = projectRepository.findOneByIndex(USER_ID_1, 0).getId();
        @NotNull String taskId = taskRepository.findOneByIndex(USER_ID_1, 0).getId();
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject("", projectId, taskId)
        );
        Assert.assertThrows(ProjectIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID_1, "", taskId)
        );
        Assert.assertThrows(TaskIdEmptyException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, "")
        );
        Assert.assertThrows(ProjectNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID_1, "123", taskId)
        );
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, "123")
        );
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, taskId);
        Assert.assertNull(taskRepository.findOneByIndex(USER_ID_1, 0).getProjectId());
    }

    @Test
    public void removeProjectById() {
        @NotNull String projectId = projectRepository.findOneByIndex(USER_ID_1, 0).getId();
        @NotNull String taskId = taskRepository.findOneByIndex(USER_ID_1, 0).getId();
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertThrows(UserIdEmptyException.class,
                () -> projectTaskService.removeProjectById("", projectId)
        );
        Assert.assertThrows(ProjectIdEmptyException.class,
                () -> projectTaskService.removeProjectById(USER_ID_1, "")
        );
        projectTaskService.removeProjectById(USER_ID_1, projectId);
        Assert.assertNull(projectRepository.findOneById(projectId));
        Assert.assertNull(taskRepository.findOneById(taskId));
    }

}
