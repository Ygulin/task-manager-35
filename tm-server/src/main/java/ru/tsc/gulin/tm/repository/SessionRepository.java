package ru.tsc.gulin.tm.repository;

import ru.tsc.gulin.tm.api.repository.ISessionRepository;
import ru.tsc.gulin.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
