package ru.tsc.gulin.tm.api.service;

import ru.tsc.gulin.tm.api.repository.IUserOwnedRepository;
import ru.tsc.gulin.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {
}
