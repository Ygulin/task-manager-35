package ru.tsc.gulin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.ISessionRepository;
import ru.tsc.gulin.tm.api.service.ISessionService;
import ru.tsc.gulin.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

}
