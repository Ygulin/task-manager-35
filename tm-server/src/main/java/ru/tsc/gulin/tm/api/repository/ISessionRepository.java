package ru.tsc.gulin.tm.api.repository;

import ru.tsc.gulin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
