package ru.tsc.gulin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class UserShowProfileRequest extends AbstractUserRequest {

    public UserShowProfileRequest(@Nullable final String token) {
        super(token);
    }

}
